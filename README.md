### To start the project with terraform:

    terraform init

### To visualize the actions to be executed with terraform

    terraform plan

### To apply terraform settings with variables

    terraform apply -var-file terraform-dev.tfvars

### To destroy the resources

    terraform destroy -target aws_vpc.myapp-vpc

### To destroy all fromtf files

    terraform destroy

### To display resource components and current state

    terraform state list

### To show current state of a specific resource/data

    terraform state show aws_vpc.myapp-vpc    

### To set avail_zone as custom tf environment variable - before apply

    export TF_VAR_avail_zone="eu-west-3a"

### To set aws configuration through env variables

    export AWS_ACCESS_KEY_ID="anaccesskey"
    export AWS_SECRET_ACCESS_KEY="asecretkey"
    export AWS_DEFAULT_REGION="us-west-2"

